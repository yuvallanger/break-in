extends Control

func _ready() -> void:
	get_node("VBoxContainer/QuitButton").visible = not OS.has_feature("HTML5")
