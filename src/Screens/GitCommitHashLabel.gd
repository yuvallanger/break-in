extends Label
"""
NOTE:
	The `res://git-hash` file still needs to be written from outside of Godot.
	Please look at my badly written Makefile for that.
	You also need to list `git-hash` in the export non-resource files filter.
"""


const GIT_HASH_FILE_NAME: String = "res://git-hash"


func _ready() -> void:
	var git_hash_file_content: String = get_file_as_text(GIT_HASH_FILE_NAME)
	text = text % git_hash_file_content


func get_file_as_text(file_name: String) -> String:
	var file: File = File.new()
	file.open(file_name, File.READ)
	return file.get_as_text()