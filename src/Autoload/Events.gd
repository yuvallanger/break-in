extends Node


# Gameplay signals
signal ball_fell
signal brick_destroyed


# HUD signals
signal player_died
signal score_updated

# Sound signals
signal create_sound_effect_ball_paddle_bump
signal create_sound_effect_ball_wall_bump
signal create_sound_effect_ball_brick_bump
