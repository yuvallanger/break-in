extends Node


var score: int = 0 setget set_score
var deaths: int = 0 setget set_deaths


func reset() -> void:
	score = 0
	deaths = 0


func set_score(value: int) -> void:
	score = value
	Events.emit_signal("score_updated")


func set_deaths(value: int) -> void:
	deaths = value
	Events.emit_signal("player_died")