extends Object
class_name BallWallBumpSound


var _phase: float = 0.0
var _samples_per_second: float
var _frequency_from: float = 600.0
var _current_frequency: float = _frequency_from
var _frequency_to: float = 400.0
var _duration: float = 0.075 # seconds
var _elapsed_time: float = 0.0


func _init(samples_per_second: float) -> void:
	_samples_per_second = samples_per_second


func sample() -> float:
	if _elapsed_time > _duration:
		return 0.0

	var our_sample: float = Sound.oscillator_saw(_phase, 8)

	_current_frequency = lerp(
		_frequency_from,
		_frequency_to,
		_elapsed_time / _duration
	)

	_elapsed_time += 1.0 / _samples_per_second
	_phase = fmod(_phase + _current_frequency / _samples_per_second, 1.0)

	return our_sample

func ended() -> bool:
	return _elapsed_time > _duration
