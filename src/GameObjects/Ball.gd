extends KinematicBody2D
class_name Ball


onready var ball_collision_shape_2d := get_node("BallCollisionShape2D")


export var speed: float = 250


var velocity = Vector2(speed, speed)


func _ready() -> void:
	global_position.x = get_viewport().get_visible_rect().size.x / 2.0

	self.update()


func is_ball_fell() -> bool:
	var viewport_y: float = get_viewport_rect().size.y
	var ball_fell: bool = self.position.y > viewport_y
	return ball_fell


func _process(delta: float) -> void:
	self.update()


func _physics_process(delta: float) -> void:
	var collision: KinematicCollision2D = move_and_collide(
		self.velocity * delta)
	if collision:
		var position: = collision.position
		print_debug("collision.position: ", position)

		var collider: = collision.collider
		self.velocity = velocity.bounce(collision.normal)
		var collider_layer: int = collider.collision_layer
		print_debug("collider_layer: ", collider_layer)
		print_debug("Ball._physics_process() time: ", OS.get_ticks_usec())

		if collider_layer & 0b1: # Paddle
			var paddle: Paddle = collider
			self.velocity.x = 10 * (self.position.x - paddle.position.x)
			Events.emit_signal("create_sound_effect_ball_paddle_bump")
			print_debug("paddle")

		if collider_layer & 0b100: # Brick
			Events.emit_signal("create_sound_effect_ball_brick_bump")

		if collider_layer & 0b1000: # Walls
			Events.emit_signal("create_sound_effect_ball_wall_bump")
			print_debug("Wall")

		if collider_layer & 0b10000: # Ceiling
			Events.emit_signal("create_sound_effect_ball_wall_bump")
			print_debug("Ceiling")

		if collider_layer & 0b100000: # Abyss
			Events.emit_signal("create_sound_effect_ball_wall_bump")
			print_debug("Abyss")


func _draw() -> void:
	var extents: Vector2 = ball_collision_shape_2d.shape.extents
	draw_rect(Rect2(-extents, 2 * extents), Color.white, false)
