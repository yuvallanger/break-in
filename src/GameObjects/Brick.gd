extends Node2D
class_name Brick


onready var animation_player: AnimationPlayer = get_node("AnimationPlayer")
onready var brick_collision_shape_2d: CollisionShape2D = get_node("BrickStaticBody2D/BrickCollisionShape2D")


export var brick_color: Color = Color(1.0, 1.0, 1.0, 1.0)
export var score_value: int = 100


func _on_Area2D_body_entered(body: Node2D):
	if body.get_name() == "Ball":
		animation_player.play("fade_out")
		PlayerData.score += score_value
		Events.emit_signal("brick_destroyed")


func _process(delta: float) -> void:
	update()


func _draw() -> void:
	var extents: Vector2 = brick_collision_shape_2d.shape.extents
	draw_rect(Rect2(-extents, 2 * extents), brick_color, false)
