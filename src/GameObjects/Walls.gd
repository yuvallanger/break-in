extends Node2D


onready var ceiling: CollisionPolygon2D = get_node("Ceiling/CeilingCollisionPolygon2D")
onready var left_wall: CollisionPolygon2D = get_node("LeftWallCollisionPolygon2D/LeftWallCollisionPolygon2D")
onready var right_wall: CollisionPolygon2D = get_node("RightCollisionPolygon2D3/RightCollisionPolygon2D")


var colors: PoolColorArray = [
	Color.black,
	Color.darkkhaki,
	Color.black,
	Color.darkkhaki,
]


func _ready() -> void:
	update()


func _process(delta: float) -> void:
	update()


func _draw() -> void:
	draw_polygon(ceiling.polygon, colors)
	draw_polygon(left_wall.polygon, colors)
	draw_polygon(right_wall.polygon, colors)


func _on_FloorArea2D_body_entered(body: PhysicsBody2D) -> void:
	if body.collision_layer & 0b10:
		Events.emit_signal("ball_fell")
