extends StaticBody2D


func _on_Area2D_body_entered(body: Node2D):
	if body.get_name() == "Ball":
		Events.emit_signal("create_sound_effect_ball_wall_bump")
