extends KinematicBody2D
class_name Paddle


onready var player_collision_shape_2d := get_node("PaddleCollisionShape2D")


export var speed: float = 600.0


func print_debug_all_display_sizes():
	print_debug("                 [Screen Metrics]")
	print_debug("            Display size: ", OS.get_screen_size())
	print_debug("   Decorated Window size: ", OS.get_real_window_size())
	print_debug("             Window size: ", OS.get_window_size())
	print_debug("        Project Settings: Width=", ProjectSettings.get_setting("display/window/size/width"), " Height=", ProjectSettings.get_setting("display/window/size/height"))


func _ready() -> void:
	# var project_setting_display_window_size_width: float = ProjectSettings.get_setting("display/window/size/width")
	#
	# global_position.x = project_setting_display_window_size_width / 2

	global_position.x = get_viewport().get_visible_rect().size.x / 2
	update()


func get_direction_input() -> Vector2:
	var movement_direction := Vector2()

	if Input.is_action_pressed("ui_right"):
		movement_direction.x += 1
	if Input.is_action_pressed("ui_left"):
		movement_direction.x -= 1
	if Input.is_action_pressed("mouse_left_click"):
		movement_direction += Vector2(
			get_global_mouse_position().x - position.x,
			0
		).normalized()

	return movement_direction


func _process(delta: float) -> void:
	update()


func _physics_process(delta: float) -> void:
	if not Engine.editor_hint:
		var movement_direction: Vector2 = get_direction_input()
		var velocity = movement_direction * speed
		move_and_collide(velocity * delta)


#func _draw() -> void:
#	var extents: Vector2 = player_collision_shape_2d.shape.extents
#	draw_rect(Rect2(-extents, 2 * extents), Color.white, false)
#	var points: PoolVector2Array = player_collision_shape_2d.shape.points
#	for point_i in range(points.size()):
#		draw_line(points[point_i], points[(point_i + 1) % points.size()], Color.white)
