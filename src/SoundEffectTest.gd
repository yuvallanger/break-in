extends Node2D


onready var total_process_calls: int = 0
onready var sound_effect_scene: PackedScene = preload("res://src/SoundEffect.tscn")


var _seconds_since_start: float = 0


func _ready() -> void:
	var Note = Sound.Note
	print_debug(Sound.note_octave_to_hertz(Note.A, 4))
	print_debug(Sound.note_octave_to_hertz(Note.C, 4))
	print_debug(Sound.note_octave_to_hertz(Note.E, 4))
#	get_tree().quit()
#	start_sound(Note.C, 4)
#	start_sound(Note.E, 4)
#	start_sound(Note.G, 4)


	Events.emit_signal("create_sound_effect", {
		"oscillator_function": "sine",
		"oscillation_per_second": Sound.note_octave_to_hertz(0, 4),
		"envelope_type": "simple",
		"attack_seconds": 0.2,
		"fade_seconds": 0.2,
		"duration_seconds": 4.0,
		"number_of_sines": 3,
	})

	Events.emit_signal("create_sound_effect", {
		"oscillator_function": "sine",
		"oscillation_per_second": Sound.note_octave_to_hertz(2, 4),
		"envelope_type": "simple",
		"attack_seconds": 0.2,
		"fade_seconds": 0.2,
		"duration_seconds": 4.0,
		"number_of_sines": 3,
	})

	Events.emit_signal("create_sound_effect", {
		"oscillator_function": "sine",
		"oscillation_per_second": Sound.note_octave_to_hertz(4, 4),
		"envelope_type": "simple",
		"attack_seconds": 0.2,
		"fade_seconds": 0.2,
		"duration_seconds": 4.0,
		"number_of_sines": 3,
	})


func _process(delta: float) -> void:
	if self._seconds_since_start > 10:
		get_tree().quit()
	self._seconds_since_start += delta
