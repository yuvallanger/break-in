extends AudioStreamPlayer


export var samples_per_second: float = 10000.0 # less samples to mix, GDScript is not super fast for this


var _playback: AudioStreamPlayback = null # object that does the actual playback
var _seconds_per_sample: float = 1.0 / samples_per_second

var _oscillators: Array = []


func _ready() -> void:
	Events.connect("create_sound_effect_ball_paddle_bump", self, "_on_create_sound_effect_ball_paddle_bump")
	Events.connect("create_sound_effect_ball_wall_bump", self, "_on_create_sound_effect_ball_wall_bump")
	Events.connect("create_sound_effect_ball_floor_bump", self, "_on_create_sound_effect_ball_floor_bump")
	Events.connect("create_sound_effect_ball_ceiling_bump", self, "_on_create_sound_effect_ball_ceiling_bump")
	Events.connect("create_sound_effect_ball_brick_bump", self, "_on_create_sound_effect_ball_brick_bump")

	stream.mix_rate = samples_per_second # setting samples_per_second is only possible before playing

	_playback = get_stream_playback()
	_fill_buffer() # prefill, do before play to avoid delay

	play()


func _on_create_sound_effect_ball_paddle_bump() -> void:
	_oscillators.append(BallPaddleBumpSound.new(samples_per_second))


func _on_create_sound_effect_ball_wall_bump() -> void:
	_oscillators.append(BallWallBumpSound.new(samples_per_second))


func _on_create_sound_effect_ball_brick_bump() -> void:
	_oscillators.append(BallBrickBumpSound.new(samples_per_second))


func _fill_buffer() -> void:
	var to_fill: int = _playback.get_frames_available()
	while (to_fill > 0):
		var sample: float = _sample_sounds()
		_playback.push_frame(Vector2.ONE * sample) # frames are stereo
		to_fill -= 1
		_remove_finished_sounds()


func _remove_finished_sounds() -> void:
	var to_delete: Array = []
	for i in range(len(_oscillators)):
		if _oscillators[i].ended():
			to_delete.push_back(i)

	for i in to_delete:
		_oscillators.remove(i)


func _sample_sounds() -> float:
	var samples_sum: float = 0.0

	for oscillator in _oscillators:
		samples_sum += oscillator.sample()

	return samples_sum / len(_oscillators) if _oscillators else 0.0


func _process(delta):
	_fill_buffer()
