extends Control


onready var deaths_label: Label = get_node("DeathsLabel")
onready var pause_overlay: ColorRect = get_node("PauseOverlay")
onready var pause_title: Label = get_node("PauseOverlay/Title")
onready var scene_tree := get_tree()
onready var score_label: Label = get_node("ScoreLabel")


var paused: bool = false setget set_paused


func _ready() -> void:
	Events.connect("score_updated", self, "update_interface")
	Events.connect("player_died", self, "_on_PlayerData_player_died")
	update_interface()


func _on_PlayerData_player_died() -> void:
	self.paused = true
	pause_title.text = "You died."


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("pause"):
		self.paused = not paused
		scene_tree.set_input_as_handled()


func update_interface() -> void:
	score_label.text = "Score: %s" % PlayerData.score
	deaths_label.text = "Deaths: %s" % PlayerData.deaths


func set_paused(value: bool) -> void:
	paused = value
	scene_tree.paused = value
	pause_overlay.visible = value
