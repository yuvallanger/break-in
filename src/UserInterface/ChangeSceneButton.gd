tool
extends Button


export(String, FILE) var next_scene_path: = ""


onready var scene_tree: = get_tree()


func _on_button_up() -> void:
	var x: int = scene_tree.change_scene(next_scene_path)
	print_debug(x)


func _get_configuration_warning() -> String:
	if not next_scene_path:
		return "next_scene_path must be set for the button to work."
	else:
		return ""