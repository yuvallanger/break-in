extends Button


onready var scene_tree := get_tree()


func _on_button_up() -> void:
	scene_tree.quit()