extends Node2D

export(String, FILE) var next_level_path: String = ""

onready var scene_tree := get_tree()
onready var brick_scene := preload("res://src/game_objects/Brick.tscn")


func lost() -> bool:
	var rect: Rect2 = get_viewport_rect()
	var ball := get_node("LevelSpace/World/Ball")
	if ball != null:
		print_debug(ball.position.y, ", ", rect.size.y)
		return ball.position.y > rect.size.y
	return false


func win() -> bool:
	var bricks_node := get_node("Bricks")
	if bricks_node.get_child(0) == null:
		return true
	return false


func _process(delta: float) -> void:
	if self.lost() or Input.is_action_just_pressed("restart"):
		PlayerData.deaths += 1
		scene_tree.reload_current_scene()
	if win():
		scene_tree.change_scene(self.next_level_path)
	print_debug(PlayerData.deaths, PlayerData.score)
	self.update()



func _get_configuration_warning() -> String:
	if not next_level_path:
		return "next_scene_path must be set for the button to work."
	else:
		return ""
		
#extends Node2D
#
#
#export(String, FILE) var next_level_path := ""
#
#
#onready var scene_tree := get_tree()
#onready var ball_node := get_node("World/Ball")
#onready var viewport := get_viewport()
#onready var viewport_rect: Rect2 = get_viewport_rect()
#
#func lost() -> bool:
#	return ball_node.position.y > viewport_rect.size.y
#
#
#func win() -> bool:
#	var bricks_node := get_node("LevelSpace/World/Bricks")
#	if bricks_node.get_child(0) == null:
#		return true
#	return false
#
#
#func _process(delta: float) -> void:
#	if self.lost() or Input.is_action_just_pressed("restart"):
#		PlayerData.deaths += 1
#		scene_tree.reload_current_scene()
#	if win():
#		scene_tree.change_scene(
#	print_debug(PlayerData.deaths, PlayerData.score)
#	self.update()