tool
extends Node2D


export(String, FILE) var next_level_path: String = ""


onready var bricks_node: = get_node("Bricks")
onready var scene_tree: = get_tree()
onready var walls: = get_node("Walls")


onready var _number_of_bricks: = bricks_node.get_child_count()


func _ready() -> void:
	Events.connect("ball_fell", self, "lose")
	Events.connect("brick_destroyed", self, "count_destroyed_brick")
#	for brick in bricks_node.get_children():
#		brick.connect("brick_destroyed", self, "count_destroyed_brick")


func lose() -> void:
	PlayerData.deaths += 1
	scene_tree.reload_current_scene()
	scene_tree.paused = false


func count_destroyed_brick() -> void:
	_number_of_bricks -= 1
	print("Brick destroyed, now %s bricks left." % _number_of_bricks)
	if _number_of_bricks <= 0:
		print("Change scene to: %s" % next_level_path)
		var change_scene_error: int = scene_tree.change_scene(next_level_path)
		print("change_scene error: %s" % change_scene_error)


func win() -> bool:
	return bricks_node.get_child(0) == null


func _process(_delta: float) -> void:
	self.update()


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("restart"):
		scene_tree.reload_current_scene()


func _get_configuration_warning() -> String:
	return "next_scene_path must be set for the button to work." if not next_level_path else ""
