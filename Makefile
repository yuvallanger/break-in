.PHONEY: godot_export
godot_export: FORCE
	godot --export HTML5 _export/html/break-in.html
	godot --export-debug HTML5 _export/html/break-in-debug.html

_export/html/git-hash: FORCE
	git diff-index --quiet HEAD -- && git rev-parse HEAD > git-hash || exit 1


FORCE: ;

upload: _export/html/git-hash godot_export
	bash upload.sh

upload_debug: godot_export
	bash upload-debug.sh
