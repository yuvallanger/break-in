#!/bin/bash

current_date="$(date --iso)"

rsync --bwlimit=300 -rvP \
	_export/html/ \
	demo.kakafarm:/var/www/kaka.farm/break-in/"$current_date"/

ssh demo.kakafarm cp -l /var/www/kaka.farm/break-in/"$current_date"/ /var/www/kaka.farm/break-in/latest
